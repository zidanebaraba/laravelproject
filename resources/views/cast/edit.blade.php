@extends('layout.master')

@section('judul')
Halaman Edit Cast
@endsection

@section('content')
    <h1>Buat Account Baru!</h1>
    <form action="/cast/{{$cast->id}}" method="post">
        @csrf
        @method('put')
        <label>First Name : </label><br>
        <input type="text" name="name" value="{{$cast->name}}"><br>
        @error('name')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
        <label>Umur : </label><br>
        <input type="number" name="umur" value="{{$cast->umur}}"><br>
        @error('umur')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
        <label>Bio : </label><br>
        <textarea name="bio" cols="30" rows="10">{{$cast->bio}}</textarea><br>
        <input type="submit" Value="Kirim Data">
    </form>
@endsection