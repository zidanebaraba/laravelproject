@extends('layout.master')

@section('judul')
Hasil Query
@endsection

@section('content')

<a href="/cast/create" class="btn btn-success">Tambah Data</a>
                

<table class="table">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Nama</th>
      <th scope="col">Umur</th>
      <th scope="col">Bio</th>
      <th scope="col">Action</th>
    </tr>
  </thead>
  <tbody>
    @forelse ($cast as $item)
        <tr>
            <td>{{$item->id}}</td>
            <td>{{$item->name}}</td>
            <td>{{$item->umur}}</td>
            <td>{{$item->bio}}</td>
            <td>
                
                <form action="/cast/{{$item->id}}" method="post">
                @method('delete')
                @csrf
                <a href="/cast/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
                <a href="/cast/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                <input type="submit" class="btn btn-danger btn-sm" name="delete" value="Delete">
                </form>
                
            </td>
        </tr>
    @empty
        <tr>
            <td>Data masih kosong</td>
        </tr>
    @endforelse
  </tbody>
</table>

@endsection